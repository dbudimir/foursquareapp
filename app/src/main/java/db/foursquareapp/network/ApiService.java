package db.foursquareapp.network;

import db.foursquareapp.network.models.VenuesResponse;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public interface ApiService {

    @GET("/v2/venues/explore")
    Observable<VenuesResponse> getRecommendedVenuesNear(@Query("near") String near, @Query("offset") int offset);
}
