package db.foursquareapp.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public class Hours {

    @SerializedName("isLocalHoliday")
    private String isLocalHoliday;

    @SerializedName("status")
    private String status;

    @SerializedName("isOpen")
    private boolean isOpen;

    public String getStatus() {
        return status;
    }
}
