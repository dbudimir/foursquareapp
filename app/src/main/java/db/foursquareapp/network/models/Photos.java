package db.foursquareapp.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public class Photos {

    @SerializedName("groups")
    private PhotoGroups[] groups;

    public PhotoGroups[] getGroups() {
        return groups;
    }
}
