package db.foursquareapp.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public class PhotoItems {

    @SerializedName("suffix")
    private String suffix;

    @SerializedName("height")
    private String height;

    @SerializedName("prefix")
    private String prefix;

    @SerializedName("width")
    private String width;

    public String getUrl() {
        return prefix + width + "x" + height + suffix;
    }
}
