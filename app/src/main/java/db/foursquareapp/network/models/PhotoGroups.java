package db.foursquareapp.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public class PhotoGroups {

    @SerializedName("count")
    private String count;

    @SerializedName("items")
    private PhotoItems[] items;

    @SerializedName("name")
    private String name;

    @SerializedName("type")
    private String type;

    public String getName() {
        return name;
    }

    public PhotoItems[] getItems() {
        return items;
    }
}
