package db.foursquareapp.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public class Groups {

    @SerializedName("items")
    private Items[] items;

    public Items[] getItems() {
        return items;
    }

    public boolean isValidResponse() {
        return items != null && items.length > 0 && items[0].isValidResponse();
    }
}
