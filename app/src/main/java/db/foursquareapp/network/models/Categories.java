package db.foursquareapp.network.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public class Categories {

    @SerializedName("id")
    private String id;

    @SerializedName("primary")
    private String primary;

    @SerializedName("name")
    private String name;

    @SerializedName("shortName")
    private String shortName;

    @SerializedName("pluralName")
    private String pluralName;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
