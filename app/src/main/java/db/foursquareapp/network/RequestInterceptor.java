package db.foursquareapp.network;

import android.support.annotation.NonNull;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import db.foursquareapp.BuildConfig;
import db.foursquareapp.utility.DateUtility;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Dario Budimir on 15/04/18.
 */

@Singleton
public class RequestInterceptor implements Interceptor {

    public static final int MAX_NUMBER_OF_ITEMS_PER_REQUEST = 20;

    @Inject public RequestInterceptor() {}

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {

        final String currentDate = DateUtility.getCurrentDateAsString();

        Request original = chain.request();
        HttpUrl originalHttpUrl = original.url();

        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter("client_id", BuildConfig.FOURSQUARE_CLIENT_ID)
                .addQueryParameter("client_secret", BuildConfig.FOURSQUARE_CLIENT_SECRET)
                .addQueryParameter("limit", Integer.toString(MAX_NUMBER_OF_ITEMS_PER_REQUEST))
                .addQueryParameter("v", currentDate)
                .addQueryParameter("venuePhotos", "1")
                .build();

        Request request = original.newBuilder().url(url).build();
        return chain.proceed(request);
    }
}
