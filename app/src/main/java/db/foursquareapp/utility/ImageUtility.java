package db.foursquareapp.utility;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.request.RequestOptions;

import db.foursquareapp.R;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public class ImageUtility {

    public static void loadImage(final String posterPath, ImageView imageView, Context context) {
        if (!TextUtils.isEmpty(posterPath)) {
            glideDrawable(posterPath, context).into(imageView);
        } else {
            imageView.setImageResource(R.drawable.placeholder);
        }
    }

    private static RequestBuilder glideDrawable(final String url, Context context) {
        return Glide
                .with(context)
                .setDefaultRequestOptions(getRequestOptions())
                .load(url);
    }

    private static RequestOptions getRequestOptions() {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.placeholder);
        requestOptions.error(R.drawable.placeholder);
        requestOptions.centerCrop();

        return requestOptions;
    }
}
