package db.foursquareapp.utility;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public class DateUtility {

    public static String getCurrentDateAsString() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.UK);
        return simpleDateFormat.format(new Date());
    }
}
