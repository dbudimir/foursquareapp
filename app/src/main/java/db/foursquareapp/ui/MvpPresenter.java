package db.foursquareapp.ui;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public interface MvpPresenter {

    void unSubscribe();
}
