package db.foursquareapp.ui;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public interface MvpView {

    void showProgressBar();

    void hideProgressBar();

    void onError(String message);
}
