package db.foursquareapp.ui.venues.presenter;


import db.foursquareapp.ui.MvpPresenter;
import db.foursquareapp.ui.venues.view.VenuesView;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public interface VenuesPresenter extends MvpPresenter {

    void getVenuesNear(final String locationName, int offset);

    void setVenuesView(VenuesView venuesView);
}
