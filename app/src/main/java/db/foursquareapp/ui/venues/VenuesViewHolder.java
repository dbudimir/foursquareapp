package db.foursquareapp.ui.venues;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import db.foursquareapp.R;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public class VenuesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.item_venue_poster)
    ImageView venuePictureView;
    @BindView(R.id.item_venue_title)
    TextView venueTitleView;
    @BindView(R.id.item_venue_type)
    TextView venueTypeView;
    @BindView(R.id.item_venue_address)
    TextView venueAddressView;
    @BindView(R.id.item_venue_status)
    TextView venueStatusView;
    @BindView(R.id.item_venue_rating)
    TextView venueRatingView;

    public VenuesViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public ImageView getVenuePictureView() {
        return venuePictureView;
    }

    public TextView getVenueTitleView() {
        return venueTitleView;
    }

    public TextView getVenueTypeView() {
        return venueTypeView;
    }

    public TextView getVenueAddressView() { return venueAddressView;}

    public TextView getVenueStatusView() { return  venueStatusView; }

    public TextView getVenueRatingView() { return venueRatingView; }
}
