package db.foursquareapp.ui.venues.presenter;

import android.content.Context;
import android.text.TextUtils;

import javax.inject.Inject;

import db.foursquareapp.R;
import db.foursquareapp.network.models.VenuesResponse;
import db.foursquareapp.ui.venues.repository.VenuesRepository;
import db.foursquareapp.ui.venues.view.VenuesView;
import db.foursquareapp.utility.NetworkUtility;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public class VenuesPresenterImpl implements VenuesPresenter {

    private Disposable subscription;
    private VenuesRepository repository;
    private VenuesView venuesView;
    private Context context;

    @Inject public VenuesPresenterImpl(VenuesRepository repository, Context context) {
        this.repository = repository;
        this.context = context;
    }

    @Override
    public void getVenuesNear(final String locationName, int page) {
        if (hasDataConnection()) {
            loadVenuesFromNetwork(locationName, page);
        } else {
            onNoDataConnection();
        }
    }

    private void loadVenuesFromNetwork(final String locationName, int page) {
        if (isViewAttached()) {
            venuesView.showProgressBar();
        }
        subscription = repository.getVenuesNear(locationName, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onVenuesSuccess, this::onVenuesFailure);
    }

    @Override
    public void setVenuesView(VenuesView venuesView) {
        this.venuesView = venuesView;
    }

    @Override
    public void unSubscribe() {
        if (isViewAttached()) {
            venuesView.hideProgressBar();
        }
        if (subscription != null && !subscription.isDisposed()) {
            subscription.dispose();
        }
    }

    private void onVenuesSuccess(final VenuesResponse venuesResponse) {
        if (isViewAttached()) {
            venuesView.hideProgressBar();
            if (venuesResponse != null) {
                venuesView.showVenues(venuesResponse.getVenues());
            }
        }
    }

    private void onVenuesFailure(Throwable e) {
        if (!hasDataConnection()) {
            onNoDataConnection();
        } else if (e != null && !TextUtils.isEmpty(e.getMessage())) {
            onServerError(e.getMessage());
        } else {
            onGenericError();
        }
    }

    private boolean hasDataConnection() {
        return NetworkUtility.hasNetworkConnection(context);
    }

    private void onGenericError() {
        if (isViewAttached()) {
            venuesView.hideProgressBar();
            venuesView.onError(context.getString(R.string.generic_error));
        }
    }

    private void onServerError(final String message) {
        if (isViewAttached()) {
            venuesView.hideProgressBar();
            venuesView.onError(message);
        }
    }

    private void onNoDataConnection() {
        if (isViewAttached()) {
            venuesView.hideProgressBar();
            venuesView.onError(context.getString(R.string.error_not_connected_to_the_internet));
        }
    }

    private boolean isViewAttached() {
        return venuesView != null;
    }
}
