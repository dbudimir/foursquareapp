package db.foursquareapp.ui.venues.repository;


import db.foursquareapp.network.models.VenuesResponse;
import io.reactivex.Observable;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public interface VenuesRepository {
    Observable<VenuesResponse> getVenuesNear(final String locationName, int offset);
}
