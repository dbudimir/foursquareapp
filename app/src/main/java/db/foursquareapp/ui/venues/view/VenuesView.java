package db.foursquareapp.ui.venues.view;

import java.util.List;

import db.foursquareapp.network.models.Venue;
import db.foursquareapp.ui.MvpView;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public interface VenuesView extends MvpView {

    void showVenues(List<Venue> venues);
}
