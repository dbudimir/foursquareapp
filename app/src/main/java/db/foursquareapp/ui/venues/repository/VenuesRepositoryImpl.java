package db.foursquareapp.ui.venues.repository;


import javax.inject.Inject;

import db.foursquareapp.network.ApiService;
import db.foursquareapp.network.RequestInterceptor;
import db.foursquareapp.network.models.VenuesResponse;
import io.reactivex.Observable;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public class VenuesRepositoryImpl implements VenuesRepository {

    private ApiService apiService;

    @Inject public VenuesRepositoryImpl(ApiService apiService) {
        this.apiService = apiService;
    }

    @Override
    public Observable<VenuesResponse> getVenuesNear(final String locationName, int page) {
        return apiService.getRecommendedVenuesNear(locationName, RequestInterceptor.MAX_NUMBER_OF_ITEMS_PER_REQUEST * page);
    }
}
