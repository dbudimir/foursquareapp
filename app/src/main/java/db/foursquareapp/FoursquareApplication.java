package db.foursquareapp;

import android.app.Application;

import db.foursquareapp.di.ApplicationComponent;
import db.foursquareapp.di.DaggerApplicationComponent;
import db.foursquareapp.di.modules.ApplicationModule;
import db.foursquareapp.di.modules.NetworkModule;

/**
 * Created by Dario Budimir on 15/04/18.
 */

public class FoursquareApplication  extends Application {

    private ApplicationComponent applicationComponent;

    @Override public void onCreate() {
        super.onCreate();

        applicationComponent = createApplicationComponent();
        applicationComponent.inject(this);
    }

    private ApplicationComponent createApplicationComponent() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule())
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}