package db.foursquareapp.di.components;


import dagger.Subcomponent;
import db.foursquareapp.di.modules.VenuesModule;
import db.foursquareapp.di.scopes.ActivityScope;
import db.foursquareapp.ui.venues.VenuesActivity;

/**
 * Created by Dario Budimir on 15/04/18.
 */

@ActivityScope
@Subcomponent(modules = {VenuesModule.class})
public interface VenuesComponent {

    void inject(VenuesActivity activity);
}
