package db.foursquareapp.di.modules;

import android.support.v7.widget.LinearLayoutManager;

import dagger.Module;
import dagger.Provides;
import db.foursquareapp.di.scopes.ActivityScope;
import db.foursquareapp.ui.venues.VenuesActivity;
import db.foursquareapp.ui.venues.VenuesAdapter;
import db.foursquareapp.ui.venues.presenter.VenuesPresenter;
import db.foursquareapp.ui.venues.presenter.VenuesPresenterImpl;
import db.foursquareapp.ui.venues.repository.VenuesRepository;
import db.foursquareapp.ui.venues.repository.VenuesRepositoryImpl;
import db.foursquareapp.ui.venues.view.VenuesView;

/**
 * Created by Dario Budimir on 15/04/18.
 */

@Module
public class VenuesModule {

    private VenuesActivity activity;
    private VenuesView view;

    public VenuesModule(VenuesActivity activity, VenuesView view) {
        this.activity = activity;
        this.view = view;
    }

    @Provides
    @ActivityScope VenuesActivity provideActivity() {
        return activity;
    }

    @Provides
    @ActivityScope VenuesView provideView() {
        return view;
    }

    @Provides
    @ActivityScope VenuesRepository provideRepository(VenuesRepositoryImpl repository) {
        return repository;
    }

    @Provides
    @ActivityScope VenuesPresenter providePresenter(VenuesPresenterImpl presenter) {
        return presenter;
    }

    @Provides
    @ActivityScope VenuesAdapter provideVenueListAdapter(VenuesActivity venuesActivity) {
        return new VenuesAdapter(venuesActivity);
    }

    @Provides
    @ActivityScope
    LinearLayoutManager provideLinearLayoutManager(VenuesActivity venuesActivity) {
        return new LinearLayoutManager(venuesActivity);
    }

}
