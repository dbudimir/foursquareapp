package db.foursquareapp.di.modules;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;
import db.foursquareapp.di.scopes.ApplicationContext;

/**
 * Created by Dario Budimir on 15/04/18.
 */

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @ApplicationContext
    Context applicationContext() {
        return application;
    }

    @Provides
    Application application() {
        return application;
    }
}
