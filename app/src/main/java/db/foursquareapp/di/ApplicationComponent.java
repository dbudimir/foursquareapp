package db.foursquareapp.di;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import db.foursquareapp.FoursquareApplication;
import db.foursquareapp.di.components.VenuesComponent;
import db.foursquareapp.di.modules.ApplicationModule;
import db.foursquareapp.di.modules.NetworkModule;
import db.foursquareapp.di.modules.VenuesModule;
import db.foursquareapp.di.scopes.ApplicationContext;

/**
 * Created by Dario Budimir on 15/04/18.
 */

@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class})
public interface ApplicationComponent {
    @ApplicationContext
    Context context();

    void inject(FoursquareApplication application);

    VenuesComponent plus(VenuesModule venuesModule);
}
