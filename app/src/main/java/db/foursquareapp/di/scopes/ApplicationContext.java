package db.foursquareapp.di.scopes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Dario Budimir on 15/04/18.
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface ApplicationContext {}
