package db.foursquareapp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import db.foursquareapp.network.ApiService;
import db.foursquareapp.ui.venues.repository.VenuesRepositoryImpl;

import static db.foursquareapp.network.RequestInterceptor.MAX_NUMBER_OF_ITEMS_PER_REQUEST;
import static org.mockito.Mockito.verify;

/**
 * Created by Dario Budimir on 15/04/18.
 */

@RunWith(MockitoJUnitRunner.class)
public class VenuesRepositoryImplTest {

    @Mock
    private ApiService apiService;

    private VenuesRepositoryImpl repository;
    private String venueName;
    private int page;

    @Before public void setUp() {
        repository = new VenuesRepositoryImpl(apiService);
        venueName = "London";
        page = 1;
    }

    @Test
    public void getVenuesTest() {
        repository.getVenuesNear(venueName, page);

        verify(apiService).getRecommendedVenuesNear(venueName, page * MAX_NUMBER_OF_ITEMS_PER_REQUEST);
    }
}
